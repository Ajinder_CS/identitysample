﻿using System;
using System.Threading.Tasks;
using Microsoft.Owin;
using Owin;
using Microsoft.Owin.Security.Cookies;

[assembly: OwinStartup(typeof(AspNetIdentitySample.App_Start.Startupconfig))]
namespace AspNetIdentitySample.App_Start
{
    public class Startupconfig
    {
         public void Configuration(IAppBuilder app)
        {
            // For more information on how to configure your application, visit http://go.microsoft.com/fwlink/?LinkID=316888
          
            // The UseCookieAuthentication tells the Asp.Net Identity framework to use the cookie based authentication.

             // We set 2 properties :
// 1. AuthenticationType : This is a string value that identifies the cookie.This is necessary since we may have several instances of the Cookie middlware.
// for example: When using external auth servers (OAuth/OpenID) the same cookie middleware is used to pass claims from the external provider.
// If we'd pulled in the "Microsoft.AspNet.Identity" Nuget package we could just use the constant "DefaultAuthenticationTypes.ApplicationCookie" which has 
//the same value - "ApplicationCookie".

//LoginPath : The path to which the user agent (browser) should be redirected to when your application returns an unauthorized (401) response. This should 
             // correspond to your "login" controller. In this case i have AuthController with a Login action.
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType ="ApplicationCookie",
                LoginPath = new PathString("/auth/login")
            });
        
        }

      

    }

}
