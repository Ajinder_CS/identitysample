﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AspNetIdentitySample
{
    public class FilterConfig
    {
        // Secure by Default.

        //It is good practise to make your application secure by default and then whitelist the controllers/actions that allow anonymous access.
        // To do this we will add the built in "AuthorizeAttribute" as a global filter.
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new AuthorizeAttribute());
        }
    }
}