﻿using AspNetIdentitySample.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;

namespace AspNetIdentitySample.Controllers
{
    public class AuthController : Controller
    {
        [AllowAnonymous]
        // GET: Auth
        public ActionResult Login(string returnUrl)
        {
               // just like the existing Forms Authentication module ,the url of the protected resource that the user 
              //attempted to access is sent as a return url querystring parameter . We need to ensure this is passed to us when 
             // the user submits the login form so it stored in the LogInModel and rendered as hidden input on the page.
            var model = new LogInModel { ReturnUrl = returnUrl };
            return View(model);
        }


        [AllowAnonymous]
        [HttpPost]
        //POST:Login
        public ActionResult Login(LogInModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }
        
            //Don't do this in production (means Static check for code).

            if (model.Email == "admin@admin.com" && model.Password == "abc123#")
            {
// 1. First we create a ClaimsIdentity object that contains information (Claims) about the current user. What's particularly interesting about the new Claims 
// based approach is that the claims are persisted to the client inside the authentication cookie.
// This means that you can add claims for frequently accessed user information rather than having to load it from a database on every request. This is similar to 
// what we had with Federated Identity's Session Authentication Module (SAM).
// Note that we also provide the authentication type. This should match the value you provided in the StartUp class.
                var identity = new ClaimsIdentity(new[]{ 
                         new Claim(ClaimTypes.Name ,"AJINDER"),
                         new Claim(ClaimTypes.Email,"Aj@dotnet.com"),
                         new Claim(ClaimTypes.Country,"India")
                 
                 }, "ApplicationCookie");

//2. Next we get obtain an IAuthenticationManager instance from the current OWIN context.This is automatically registered for you during startup .

                var ctx = Request.GetOwinContext();
                var authManager = ctx.Authentication;
//3. Then we call the IAuthenticationManager.SignIn passing the claims Identity.This sets the authentication cookie on the client.
                authManager.SignIn(identity);
//4.Finally we redirect the user agent to the resource they attempted to access. We also check to ensure the return Url is local to the application to prevent Open
                //redirection attacks.
                return Redirect(GetRedirectUrl(model.ReturnUrl));
            }

            //user auth.failed
            ModelState.AddModelError("", "Invalid email or passoword ");

            return View();
        }

        public ActionResult Logout()
        {
            // Again we obtain the IAuthenticationManager instance from the OWIN context, this time calling SignOut passing 
            // the authentication type (so the manager knows exactly what cookie to remove).
            var ctx = Request.GetOwinContext();
            var authManager = ctx.Authentication;

            authManager.SignOut("ApplicationCookie");

            return RedirectToAction("Index", "Home");
        }

        private string GetRedirectUrl(string returnUrl)
        {
            if (string.IsNullOrEmpty(returnUrl) || !Url.IsLocalUrl(returnUrl))
            {
                return Url.Action("index", "home");
            }

            return returnUrl;
        }
    }
}