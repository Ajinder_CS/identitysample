﻿using AspNetIdentitySample.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;

namespace AspNetIdentitySample.Controllers
{
    public class HomeController : AppController
    {
        // GET: Home
        public ActionResult Index()
        {
            // Accessing custom claim data 

            //Whilst claim based principles/Identities have been used in Asp.Net for a while ,the current user is still exposed as 
            // "IPrincipal/IIdentity" this means all we can really get from the User property in our controllers and views 
            //is the "Name" claim.

            // To access the other claim else we can cast the current user identity as a ClaimIdentity.

    //        var claimsIdentity = User.Identity as ClaimsIdentity;
  //          ViewBag.Country = claimsIdentity.FindFirst(ClaimTypes.Country).Value;

            // But having to do the same everytime we need to access to user claims is a bit of a smell.Wouldn't it be nice if we 
            //had strongly typed access to your user claims ?

            //To do this we will create our own Principal class that wraps the current ClaimsPrincipal and provides strongly typed 
            //properties to it's underlying claims.

           // ViewBag.Country = CurrentUser.Country;

            // Much nicer . But we can still do better .Why do we need to add this information to ViewBag when we already have access 
           // to the current user within our view ?

            // Let's create a custom base view page for our Razor views that provides access to our "AppUser" principal.

            return View();
        }
    }

 
}